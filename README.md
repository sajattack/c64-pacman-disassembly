# C64 Pacman Disassembly
This repository is a radare2 project folder of my Commodore 64 Pac-Man disassembly. The cartridge file that is disassembled is 
not included for copyright reasons. Only the metadata of my disassembly is here.
The zip file containing the cartridge that was disassembled has an MD5 checksum of be028b178172c85054b82275ff72ca85
The cartridge itself has an MD5 checksum of e45489ea7069e9a3bcee1b97a14e26d7
What you do with that information is up to you.
